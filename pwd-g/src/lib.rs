#![deny(clippy::pedantic)]
pub mod password_generator;

extern crate rand;
extern crate serde;
extern crate serde_json;
