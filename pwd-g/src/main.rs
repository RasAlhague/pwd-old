extern crate clipboard;
extern crate rand;
extern crate serde;
extern crate serde_json;

use pwd_g::password_generator::{Generator, PwdSettings};

fn main() {
    println!(
        "{} v.{}\n",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );

    let settings = PwdSettings::new();
    Generator::run(&settings);
}
