# pwd-m
A passwort manager for the commandline written in rust.

## Planned
 - Password generation
    - copy to clipboard
    - changeable password charsets
    - auto clear clipboard after given time
    - safe password into a file
    - safe password into the manager
 - Linux and windows support
 - Choosable safe encryption
 - Masterkey for the manager
 - posibility to set simple pins for password access