use secrecy::Secret;

pub struct PwdManager<'a> {
    files_path: &'a str,
}

// pub impl
impl<'a> PwdManager<'a> {
    #[must_use]
    pub fn read(&self, password_identifier: &str) -> Result<Secret<String>, String> {
        Ok(Secret::new(String::from("test")))
    }

    pub fn write(&self, password_identifier: &str) -> Result<(), String> {
        Ok(())
    }

    pub fn update(&self, password_identifier: &str) -> Result<(), String> {
        Ok(())
    }

    pub fn delete(&self, password_identifier: &str) -> Result<(), String> {
        Ok(())
    }
}

// private impl
impl<'a> PwdManager<'a> {

}