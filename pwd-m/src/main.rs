use pwd_g::password_generator::{Generator, PwdSettings};
use structopt::StructOpt;

fn main() {
    println!(
        "{} v.{}\n",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );

    let start_up = StartUp::new();

    run(start_up);
}

fn run(start_up: StartUp) {
    match start_up {
        StartUp::Generate(settings) => generate_password(settings),
        StartUp::Get { password_name } => get_password(&password_name),
        StartUp::Add { password_name } => add_password(&password_name),
        StartUp::Remove { password_name } => remove_password(&password_name),
        StartUp::UpdateMaster => update_master(),
    };
}

fn generate_password(settings: PwdSettings) {
    Generator::run(&settings);
}

fn get_password(pwd_name: &str) {
    print!("");
}

fn add_password(pwd_name: &str) {}

fn remove_password(pwd_name: &str) {}

fn update_master() {}

fn check_master_pwd() -> bool {
    true
}


#[derive(StructOpt)]
#[structopt(about = "the stupid content tracker")]
enum StartUp {
    #[structopt(about = "generates passwords")]
    Generate(PwdSettings),
    #[structopt(about = "gets a password from the manager")]
    Get {
        #[structopt(
            short = "p",
            long = "password_name",
            help = "The name under which the password was saved."
        )]
        password_name: String,
    },
    #[structopt(about = "adds a new password to the manager")]
    Add {
        #[structopt(
            short = "p",
            long = "password_name",
            help = "The name under which the password was saved."
        )]
        password_name: String,
    },
    #[structopt(about = "removes a password from the manager")]
    Remove {
        #[structopt(
            short = "p",
            long = "password_name",
            help = "The name under which the password was saved."
        )]
        password_name: String,
    },
    #[structopt(about = "updates the master password")]
    UpdateMaster,
}

impl StartUp {
    fn new() -> StartUp {
        StartUp::from_args()
    }
}
